import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import pandas as pd
import numpy as np
import json


url_template = "https://www.otomoto.pl/osobowe/skoda/octavia?search%5Bfilter_enum_damaged%5D=0&search%5Bfilter_" \
               "enum_fuel_type%5D=diesel&search%5Bfilter_enum_generation%5D=gen-iii-2013&page={}" \
               "&search%5Badvanced_search_expanded%5D=true"


def get_car_offer_pages_html(url_base, num_pages):
    # Loop through pages
    content_all_pages = []
    for page_number in range(1, num_pages + 1):
        # Create the URL for the current page
        url = url_base.format(page_number)

        # Send HTTP request
        response = requests.get(url)

        if response.status_code == 200:
            # Parse the HTML content
            soup = BeautifulSoup(response.content, 'html.parser')

            # Process the content or save it to a file
            content = soup.prettify()  # You can customize this based on your needs
            content_all_pages.append(content)

            print(f"Page {page_number} downloaded successfully.")
        else:
            print(f"Failed to download page {page_number}. Status code: {response.status_code}")

    return content_all_pages


def get_links_to_offers(html_code):
    # Parse the HTML content
    soup = BeautifulSoup(html_code, 'html.parser')

    # Find all the links within the specified HTML structure
    offer_links = soup.select('div.e1oqyyyi6 h1.e1oqyyyi9 a')

    links_to_offers = []
    # Extract and print the links only if they are from the otomoto.pl domain
    for link in offer_links:
        offer_url = link['href']
        parsed_url = urlparse(offer_url)

        if parsed_url.netloc == 'www.otomoto.pl':
            links_to_offers.append(offer_url)
    return links_to_offers


def get_all_car_offer_links_from_html(html_pages):
    all_links = []
    for html_code in html_pages:
        all_links.append(get_links_to_offers(html_code))

    all_links = [link for sub_list in all_links for link in sub_list]
    return all_links


def get_car_offers_links_from_search_url(url, num_pages):
    html_pages = get_car_offer_pages_html(url, num_pages)
    links = get_all_car_offer_links_from_html(html_pages)

    return links

def load_offer(offer_url):
    response = requests.get(offer_url)
    if response.status_code == 200:
        # Parse the HTML content
        soup_tmp = BeautifulSoup(response.content, 'html.parser')
        content = soup_tmp.prettify()

    soup = BeautifulSoup(content, 'html.parser')
    return soup

def get_offer_params(soup):
    keys_to_extract = ['year',
                       'mileage',
                       'engine_capacity',
                       'fuel_type',
                       'engine_power',
                       'gearbox',
                       'door_count',
                       'colour_type',
                       'private_business',
                       'price_amount',
                       'price_gross_net'
                       ]

    # Find the script tag containing the __NEXT_DATA__ identifier
    script_tag = soup.find('script', {'id': '__NEXT_DATA__'})

    # Extract the content of the script tag
    script_content = script_tag.contents[0] if script_tag else None

    # Parse the JSON data within the script content
    data = json.loads(script_content)

    # Access specific information
    specific_info = data.get('props', {}).get('pageProps', {}).get('advert', {}).get('parametersDict', {})

    results_tmp = {key: specific_info.get(key) for key in keys_to_extract}
    results = {key: value['values'][0]['value'] if value else None for key, value in results_tmp.items()}
    return results, specific_info


def get_offer_attr(soup):
    equipment_containers = soup.find_all('div', {'class': 'ooa-1rv4f1f e1ic0wg13'})

    equipment_items = []
    # Iterate through each container
    for container in equipment_containers:
        # Find the <p> element within the container
        equipment_element = container.find('p', {'class': 'e1ic0wg14 ooa-1i4y99d er34gjf0'})
        # If the <p> element is found, append its text to the list

        if equipment_element:
            equipment_items.append(equipment_element.text.strip())

    return equipment_items


def process_offer(offer_url):
    try:
        soup = load_offer(offer_url)
        offer_params = get_offer_params(soup)
        offer_attr = get_offer_attr(soup)
        price = get_price(soup)
    except:
        print(f"Error occured for link {offer_url}")
        return None

    return offer_params, offer_attr, price, offer_url


def get_price(soup):
    # Find the element containing the price
    price_element = soup.find('h3', {'class': 'offer-price__number'})

    # Extract the price text
    price_text = price_element.text.strip() if price_element else 'Price not found'

    return price_text

def run(url, num_pages):
    # offers = get_car_offer_pages_html(url, num_pages)
    links = get_car_offers_links_from_search_url(url, num_pages)
    results = [process_offer(link) for link in links]

    return results