import pandas as pd
import numpy as np
from sklearn.preprocessing import MultiLabelBinarizer


def transform_raw_data(df):
    # Select only appropriate columns
    cols_to_take = ['year',
                    'mileage',
                    'engine_capacity',
                    'fuel_type',
                    'engine_power',
                    'gearbox',
                    'colour_type',
                    'private_business',
                    'price_amount',
                    'price_gross_net'
                    ]

    numeric_cols = ['year',
                    'mileage',
                    'engine_capacity',
                    'engine_power',
                    'price_amount'
                    ]

    # preselect columns to consider
    X = df[cols_to_take]

    # change columns to numeric type
    X[numeric_cols] = X[numeric_cols].astype(float)

    # Modify engine capacity - find the closest correct value
    X['engine_capacity'] = X['engine_capacity'].apply(correct_engine_capacity)

    return X


def correct_engine_capacity(value):
    valid_values = [1968, 1598]

    # Convert the input value to an integer
    value = int(value)

    # Find the closest valid value
    closest_valid = min(valid_values, key=lambda x: abs(x - value))

    return closest_valid


def transform_equipment_to_df(list_of_equipment):
    mlb = MultiLabelBinarizer()

    # Fit and transform the data to get one-hot encoding
    one_hot_encoded = mlb.fit_transform(list_of_equipment)

    # Create a DataFrame with one-hot encoding
    equipment_df = pd.DataFrame(one_hot_encoded, columns=mlb.classes_)
    return equipment_df