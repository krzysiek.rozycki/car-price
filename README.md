# Car-price

## Description

This is a Python project that is supposed to be used to predict car price. It downloads data and then uses it inside Azure ML to train regression model.

## Installation

1. Clone the repository:

   ```bash
   git clone git@gitlab.com:krzysiek.rozycki/car-price.git